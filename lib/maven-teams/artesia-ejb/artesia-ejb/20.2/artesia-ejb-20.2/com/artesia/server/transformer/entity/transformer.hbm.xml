<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC
      "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
          "http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd">

<hibernate-mapping package="com.artesia.server.transformer.entity">
	<typedef class="com.artesia.server.common.entity.TeamsIdentifierType" name="teams_id" />

   <!-- =====================
        Entity Table Mappings
        ===================== -->

   <!-- Transformers -->

  <class name="com.artesia.server.transformer.entity.TransformerInfoEntity" 
    table="TRANSFORMERS">
    <id name="id" column="TRANSFORMER_ID" type="string">
      <generator class="assigned"/>
    </id>
    <property name="implementationClass" column="IMPLEMENTATION_CLASS" type="string"/>       
    <component name="transformerInfo" class="com.artesia.transformer.TransformerInfo">
       <property name="name" column="NAME" type="string"/>
       <property name="description" column="DESCR" type="string"/>
       <property name="executable" column="TRANSFORMER_EXECUTABLE" type="string"/>
       <property name="public" column="IS_PUBLIC" type="yes_no"/>
       <property name="importTransformer" column="IS_IMPORT" type="yes_no"/>
       <property name="exportTransformer" column="IS_EXPORT" type="yes_no"/>
       <property name="exportJobTransformer" column="IS_EXPORT_JOB" type="yes_no"/>
		<property name="namedLocationTransformer" column="IS_NAMED_LOCATION" type="yes_no"/>
       <property name="legacyId" column="ID" type="int" update="false"/>
       <property name="transformerType" column="TRANSFORMER_TYPE" type="string"/>
       <!-- using Integer since this is a nulable field -->
       <property name="filterMask" column="FILTER_MASK" type="java.lang.Integer"/>
       <property name="wrapperId" column="WRAPPER_ID" type="java.lang.Integer"/>
       <property name="userId" column="USER_ID" type="string"/>
       <property name="updateId" column="UPDATE_ID" type="string" />
       <property name="updateDate" column="UPDATE_DT" type="timestamp" />
       <property name="workingDir" column="WORKING_DIR" type="string" />
       <property name="locked" column="IS_LOCKED" type="yes_no" formula="case when IS_LOCKED is null then 'N' else 'Y' end"/>
       <property name="systemTransformer" column="IS_SYSTEM" type="yes_no"/>
    </component>
    <!-- this is in the entity instead of the component because the map in the component will
  	  need to be sync'd manually with the transfer object, otherwise we would be transfering hibernate
  	  map objects -->
  	 <map
  	    name="configurationAttributesMap"
  	    table="TRANSFORMERS_CONFIG_ATTR"
  	    lazy="true"
  	    batch-size="4"
  	    cascade="all"
  	    >
  	    <key column="TRANSFORMER_ID"/>
  	    <index column="ATTR_NAME" type="string"/>
  	    <element column="ATTR_VALUE" type="string"/>
  	 </map>
  </class>

   <!-- Transformer Argument -->

   <class name="com.artesia.server.transformer.entity.TransformerArgumentEntity"
     table="TRANSFORMER_ATTRIBUTES" >

      <id name="id" column="ID" type="long">
         <generator class="assigned"/>
      </id>

      <property name="legacyTransformerId"  column="LEGACY_TRANSFORMER_ID" type="long"/>
      <property name="transformerId"  column="TRANSFORMER_ID" type="string"/>
      <property name="name"           column="NAME"           type="string"/>
      <property name="description"    column="DESCR"          type="string"/>
      <property name="dataTypeId"     column="DATA_TYPE_ID"   type="long"/>
      <property name="optionTypeId"   column="OPTION_TYPE_ID" type="long"/>
      <property name="displayId"      column="DISPLAY_ID"     type="long"/>
      <property name="defaultValue"   column="DEFAULT_VALUE"  type="string"/>
      <property name="required"       column="REQUIRED"       type="string"/>
      <property name="argumentNumber" column="ARG_NUM"        type="int"/>
      <property name="argumentOption" column="ARG_OPTION"     type="string"/>
      <property name="guiNumber"      column="GUI_NUM"        type="int"/>
      <property name="guiComments"    column="GUI_COMMENTS"   type="string"/>
      <property name="maximumLength"  column="MAX_LENGTH"     type="int"/>

   </class>

   <!-- Argument Option Type -->

   <class name="com.artesia.server.transformer.entity.ArgumentOptionTypeEntity"
     table="ATTR_OPTION_TYPES" mutable="false" >

      <id name="id" column="ID" type="long">
         <generator class="assigned"/>
      </id>

      <property name="name"        column="NAME"  type="string"/>
      <property name="description" column="DESCR" type="string"/>

   </class>

   <!-- Argument Data Type -->

   <class name="com.artesia.server.transformer.entity.ArgumentDataTypeEntity"
     table="ATTR_DATA_TYPES" mutable="false" >

      <id name="id" column="ID" type="long">
         <generator class="assigned"/>
      </id>

      <property name="name"        column="NAME"  type="string"/>
      <property name="description" column="DESCR" type="string"/>

   </class>

   <!-- Argument Display -->

   <class name="com.artesia.server.transformer.entity.ArgumentDisplayEntity"
     table="ATTR_DISPLAYS" mutable="false" >

      <id name="id" column="ID" type="long">
         <generator class="assigned"/>
      </id>

      <property name="name"        column="NAME"  type="string"/>
      <property name="description" column="DESCR" type="string"/>

   </class>

   <!-- Export Transformer Content Type Map -->

   <class name="com.artesia.server.transformer.entity.ExportTransformerContentTypeEntryEntity"
     table="EXPORT_TRANS_CONTENT_TYPE_MAP" >

      <composite-id >
        <key-property name="transformerId" column="TRANSFORMER_ID"  type="string" />
        <key-property name="contentType"   column="CONTENT_TYPE"    type="string" />
      </composite-id >

      <property name="sequenceNumber" column="SEQ_NUMBER" type="int"/>

   </class>

   <!-- Transformer Attribute Value -->

   <class name="com.artesia.server.transformer.entity.TransformerArgumentValueEntity"
     table="LIST_VALUES">

      <composite-id >
        <key-property name="transformerId"           column="TRANSFORMER_ID"        type="string"   />
        <key-property name="legacyTransformerId"           column="LEGACY_TRANSFORMER_ID"        type="long"   />
        <key-property name="transformerArgumentId"  column="TRANSFORMER_ATTR_ID"   type="long"   />
        <key-property name="name"                    column="NAME"                  type="string" />
      </composite-id >

      <property name="value" column="VALUE" type="string"/>

   </class>
   
   
   <class
	name="com.artesia.server.transformer.entity.TransformationLogEntity"
	table="OTMM_TRANSFORMATIONS_DETAILS">
	 <id name="jobId" column="JOB_ID" type="string">
         <generator class="assigned"/>
      </id>
     <component name="jobDetails"
			class="com.artesia.transformer.TransformationJobDetails">
			<property name="assetId" column="UOI_ID" type="string" update = "false"/>
			<property name="transformationJobData" column="JOB_DATA" type="string" update="false" />
			<property name="transformationRequest" column="REQUEST" type="string"  update="false"/>
			<property name="transformationResponse" column="RESPONSE" type="string"/>
			<property name="status" column="STATUS" type="string" />
			<property name="jobInstanceId" column="SUB_JOB_ID" type="long" update="false"/>
			<property name="otmmJobId" column="OTMM_JOB_ID" type="long" update="false"/>
			<property name="submitTime" column="SUBMIT_TIME" type="timestamp" update="false"/>
			<property name="completeTime" column="COMPLETION_TIME" type="timestamp"/>
			<property name="transformationType" column="TRANSFORMATION_TYPE" type="string" update="false"/>
	</component>
  </class>
   
   <!-- Import Transformer Mime Type Mapping -->
   <class name="com.artesia.server.transformer.entity.ImportTransformerMimeTypeConfigurationsEntity" table="IMPORT_TRANS_MIME_TYPE_MAP">
		<composite-id name="importTransformerMimeTypeConfigurationKey" class="com.artesia.transformer.ImportTransMimeTypeConfKey">
			<key-property name="transformerID" column="TRANSFORMER_ID" type="teams_id"/>
			<key-property name="mimeType" column="MIME_TYPE" type="string"/>
		</composite-id>
		<property name="sequenceNumber" column="TRANSFORMER_SEQ" type="int"></property>
		<property name="contentPreProcessorClass" column="CONTENTPREPROCESS_CLASS" type="string"></property>
	</class>
	
	<!-- Contact Style Sheet -->
	<class
		name="com.artesia.server.transformer.entity.ContactStyleSheetEntity"
		table="OTMM_CONTACTSHEET_TEMPLATES">
		<id name="fileName" column="FILE_NAME" type="string">
			<generator class="assigned"/> 
		</id>
		<component name="styleSheet"
			class="com.artesia.transformer.ContactStyleSheet">
			<property name="customContent" column="CUSTOM_CONTENT"
				type="string" />
		</component>
	</class>
	

   <!-- =============
        Named Queries
        ============= -->
   
   <!-- Get Transformer Content Types -->
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_EXPORT_TRANSFORMER_CONTENT_TYPE_MAP_QUERY">
     <![CDATA[
     from com.artesia.server.transformer.entity.ExportTransformerContentTypeEntryEntity et
     order by et.contentType, et.sequenceNumber
     ]]>
   </query>
   
   <query name="com.artesia.server.transformer.entity.TransformerMappingStorageManager.GET_MAX_SEQUENCE_NUMBER">
     <![CDATA[
     select max(et.sequenceNumber) from com.artesia.server.transformer.entity.ExportTransformerContentTypeEntryEntity et
     where et.contentType = :contentType
     ]]>
   </query>
   
   <query name="com.artesia.server.transformer.entity.TransformerMappingStorageManager.RETRIEVE_TRANSFORMER_FOR_CONTENT_TYPE">
     <![CDATA[
     select transInfo from com.artesia.server.transformer.entity.ExportTransformerContentTypeEntryEntity et, 
     com.artesia.server.transformer.entity.TransformerInfoEntity transInfo where et.transformerId = transInfo.id and et.contentType = :contentType order by et.sequenceNumber
     ]]>
   </query>
   
   <!--
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_TRANSFORMER_CONTENT_TYPES">
     <![CDATA[
     from com.artesia.server.transformer.entity.TransformerContentTypeEntity contentType
     where contentType.transformerId = :legacyId
     ]]>
   </query>
-->

   <!-- Get Attribute Data Types -->
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_ARG_DATA_TYPES">
     <![CDATA[
     from com.artesia.server.transformer.entity.ArgumentDataTypeEntity
     ]]>
   </query>

   <!-- Get Attribute Displays -->
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_ARG_DISPLAYS">
     <![CDATA[
     from com.artesia.server.transformer.entity.ArgumentDisplayEntity
     ]]>
   </query>

   <!-- Get Attribute Option Types -->
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_ARG_OPTION_TYPES">
     <![CDATA[
     from com.artesia.server.transformer.entity.ArgumentOptionTypeEntity
     ]]>
   </query>

   <!-- Get Transformer Attribute Values -->
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_ARG_VALUES">
     <![CDATA[
     from com.artesia.server.transformer.entity.TransformerArgumentValueEntity value
     where value.transformerId = :transformerId  and value.transformerArgumentId = :transformerArgumentId order by value.name asc
     ]]>
   </query>

   <!-- Get All Transformers -->
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_ALL_TRANSFORMERS">
     <![CDATA[
     from com.artesia.server.transformer.entity.TransformerInfoEntity xform
     ]]>
   </query>
   
   <!-- Get All Transformers -->
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_ALL_TRANSFORMER_IDS">
     <![CDATA[
     select xform.id from com.artesia.server.transformer.entity.TransformerInfoEntity xform
     ]]>
   </query>
   
   
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.DELETE_TRANSFORMER_ARGUMENT_BY_ID">
   	<![CDATA[
   		DELETE FROM 
   			com.artesia.server.transformer.entity.TransformerArgumentEntity transformerArguments 
   		WHERE 
   			transformerArguments.id IN :transformerArgumentIds
   	]]>
   </query>
   
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.DELETE_TRANSFORMER_ARGUMENT_BY_TRANSFORMER_ID">
   	<![CDATA[
   		DELETE FROM 
   			com.artesia.server.transformer.entity.TransformerArgumentEntity transformerArguments 
   		WHERE 
   			transformerArguments.transformerId IN :transformerIds
   	]]>
   </query>
   
   <query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_TRANSFORMER_BY_LEGACY_ID">
   	<![CDATA[
   		FROM 
   			com.artesia.server.transformer.entity.TransformerInfoEntity transformer 
   		WHERE 
   			transformer.id IN :transformerId
   	]]>
   </query>
   
   <query name="com.artesia.server.transformer.entity.TransformerMappingStorageManager.GET_MAX_SEQUENCE_NUMBER_IMPORT_MIME_TYPE_MAP">
     <![CDATA[
     SELECT MAX(importMimeTypeEntity.sequenceNumber) FROM 
     	com.artesia.server.transformer.entity.ImportTransformerMimeTypeConfigurationsEntity importMimeTypeEntity
     WHERE 
     	importMimeTypeEntity.importTransformerMimeTypeConfigurationKey.mimeType =:mimeType
     ]]>
   </query>
   
   <query name="com.artesia.server.transformer.entity.TransformerMappingStorageManager.GET_EXPORT_CONTENT_TYPE_MAPPING_BASED_ON_CONTENT_TYPE_IN_SEQUENCE_NUMBER_ASCENDING_ORDER">
   	<![CDATA[
   		FROM 
   			com.artesia.server.transformer.entity.ExportTransformerContentTypeEntryEntity contentTypeEntry
   		where 
   			contentTypeEntry.contentType = :contentType
   		order by
   			contentTypeEntry.sequenceNumber asc
     ]]>
   </query>
   
   <query name="com.artesia.server.transformer.entity.TransformerMappingStorageManager.GET_IMPORT_TRANSFORMER_MIME_TYPE_MAP_BASED_ON_MIME_TYPE_ORDER_BY_SEQUENCE_NO">
   	<![CDATA[
   		FROM 
   			com.artesia.server.transformer.entity.ImportTransformerMimeTypeConfigurationsEntity entity
   		where 
   			entity.importTransformerMimeTypeConfigurationKey.mimeType = :mimeType
   		order by
   			entity.sequenceNumber asc
   	]]>
   </query>

	<query
		name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_TRANSFORMER_BY_NAME_AND_ID">
   	<![CDATA[
   		FROM 
   			com.artesia.server.transformer.entity.TransformerInfoEntity transformer 
   		WHERE 
   			transformer.transformerInfo.name = :transformerName or
   			transformer.id = :transformerId
   	]]>
	</query>

	<query
		name="com.artesia.server.transformer.entity.TransformerStorageManager.DELETE_LOGS_BY_JOB_IDS">
   	<![CDATA[
   		DELETE FROM 
   			com.artesia.server.transformer.entity.TransformationLogEntity transformerLogs 
   		WHERE 
   			transformerLogs.jobDetails.otmmJobId in (:jobIds)
   	]]>
	</query>
	
	<query name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_CONTACT_STYLE_SHEET">
	<![CDATA[
		FROM 
			com.artesia.server.transformer.entity.ContactStyleSheetEntity styleSheetEntity
		WHERE
			styleSheetEntity.fileName = :fileName
	]]>
	</query>

	<query
		name="com.artesia.server.transformer.entity.TransformerStorageManager.RETRIEVE_CONTACT_STYLE_SHEETS">
	<![CDATA[
		FROM 
			com.artesia.server.transformer.entity.ContactStyleSheetEntity styleSheetEntity
		ORDER BY 
			styleSheetEntity.fileName 
	]]>
	</query>

	<query
		name="com.artesia.server.transformer.entity.TransformerStorageManager.UPDATE_CONTACT_STYLE_SHEET">
	<![CDATA[
		UPDATE 
			com.artesia.server.transformer.entity.ContactStyleSheetEntity styleSheetEntity
		SET
			styleSheetEntity.styleSheet.customContent = :customContent
		WHERE 
		 	styleSheetEntity.fileName = :fileName 
	]]>
	</query>

	<query
		name="com.artesia.server.transformer.entity.TransformerStorageManager.DELETE_CONTACT_STYLE_SHEET">
		<![CDATA[
			DELETE FROM 
				com.artesia.server.transformer.entity.ContactStyleSheetEntity styleSheetEntity
			WHERE
				styleSheetEntity.fileName = :fileName 
		]]>
	</query>   
</hibernate-mapping>