package com.piaggio.dam.transformers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.artesia.security.SecuritySession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.CascadingDomainValue;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;

public class AcceSpareParts extends DamCommon {

	private static final Log log = LogFactory.getLog(AcceSpareParts.class);
	private HashMap<Integer,String> acceSparePartsModel = new HashMap<Integer,String>();
	 private MetadataField productCode = new MetadataField();
	 private MetadataField family = new MetadataField();
	 private MetadataField model = new MetadataField();
	 private MetadataField color = new MetadataField();
	 private MetadataField size = new MetadataField();
	 private MetadataField view = new MetadataField();
	 private MetadataField height = new MetadataField();
	 private MetadataField width = new MetadataField();
	 private MetadataField sapAttribute = new MetadataField();
	 private MetadataField market = new MetadataField();
	 private MetadataField status = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();
	 private static String FAMILY_BRAND_CODE = "PIAGGIO.FIELDS.PROVA.CASCADING";
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation ( String modelRead, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) {
		
		String acceSpareParts = piaggioProperties.getProperty("acceSpareParts");
		log.info("prepareStructureToSetMetadataInformation-acceSpareParts: " + acceSpareParts);
		acceSparePartsModel.put(1, "PIAGGIO.MODEL.ACC.SPAREPARTS");
		
		
		Integer key = -1;
		boolean end = false;
		int numberOfPcmMetadata = 0;
		for(Integer setKey : acceSparePartsModel.keySet()) {
			
			if (modelRead.equals(acceSparePartsModel.get(setKey)) == true && !end == true) {
	    		  key = setKey;
	    		  end = true;
	    		  
	    		  HashMap<Integer,MetadataField> metadataFields = new HashMap<Integer,MetadataField>(); 
	    		  HashMap<Integer,String> modelspiaggio = new HashMap<Integer,String>(); 
	    		  HashMap<Integer,String> piaggioTabularFields = new HashMap<Integer,String>(); 
	    		  
	    		  String codiceprodotto;
         	  	  String marca;
         	      String famiglia;
         	      String modello;
         	      String colore;
         	      String taglia;
         	      String vista;
         	      String altezza;
         	      String larghezza;
         	      String attributoSap;
         	      String mercato;
         	      String status;
         	      String idColor;
         	      String idSize;
         	      String idView;
         	      String idSapAttribute;
         	      String idMarket;
         	      String idFamily;
         	      String idModel;
         	      String idBrand;
         	      
         	      
         	 switch(key) {
	    			
	  	  			case 1:	
	  	  				
		  	  			 metadataFields.put(1, productCode);
						 metadataFields.put(2, family);
						 metadataFields.put(3, model);
		          	  	 metadataFields.put(4, market);
		          	  	 metadataFields.put(5, color);
		          	  	 metadataFields.put(6, size);
						 metadataFields.put(7, view);
						 metadataFields.put(8, height);
		          	  	 metadataFields.put(9, width);
		          	  	// metadataFields.put(10, market);
		          	  	 
		          	  	 /*metadataFields.put(11, sapAttribute);
		          	  	 metadataFields.put(12, market);
		          	  	 metadataFields.put(13, status);*/
		
		          	  	 codiceprodotto = piaggioProperties.getProperty("codiceprodotto");
		          	  	 marca = piaggioProperties.getProperty("marca");
		          	  	 famiglia = piaggioProperties.getProperty("famiglia");
		          	     modello = piaggioProperties.getProperty("modello");
		          	     colore = piaggioProperties.getProperty("colore");
		          	     taglia = piaggioProperties.getProperty("taglia");
		          	     vista = piaggioProperties.getProperty("vista");
		          	     altezza = piaggioProperties.getProperty("altezza");
		          	     larghezza = piaggioProperties.getProperty("larghezza");
		          	     attributoSap = piaggioProperties.getProperty("attributoSap");
		          	     mercato = piaggioProperties.getProperty("mercato");
			          	 status = piaggioProperties.getProperty("status");
		          	     
			          	/*idColor = piaggioProperties.getProperty("idMercato");
			          	idSize = piaggioProperties.getProperty("idSize");
			          	idView = piaggioProperties.getProperty("idView");
			          	idSapAttribute= piaggioProperties.getProperty("idSapAttribute");
			          	idMarket = piaggioProperties.getProperty("idMarket");
			          	idFamily =piaggioProperties.getProperty("idFamily");
			          	idModel= piaggioProperties.getProperty("idModel");
			          	idBrand= piaggioProperties.getProperty("idBrand");*/
			          	idColor = "PIAGGIO.LOOKUP.COLOR";
			          	idSize = "PIAGGIO.LOOKUP.PRODUCT SIZE";
			          	idView = "PIAGGIO.LOOKUP.PRODUCT VIEW";
			          	//idSapAttribute= piaggioProperties.getProperty("idSapAttribute");
			          	//idMarket = piaggioProperties.getProperty("idMarket");
			          	idFamily ="PIAGGIO.LOOKUP.PRODUCT FAMILY";
			          	idModel= "PIAGGIO.LOOKUP.PRODUCT MODEL";
			          	idBrand= "PIAGGIO.LOOKUP.BRAND VALUES";
		          	     
		          	  	 //marketTableTabular = piaggioProperties.getProperty("marketTableTabular");
		          	  	 //marketTabularFieldId = piaggioProperties.getProperty("marketTabularFieldId");
		          	  
			          	 idBrand = getDomainIdByDomainDescription (securitySession,metadata[1],idBrand);
			          	 log.info("idBrand: " + idBrand);
	         	         metadata[1] = idBrand;
	         	         idFamily = getDomainIdByDomainDescription (securitySession,metadata[2], idFamily);
	         	         log.info("idFamily: " +idFamily);
	         	         metadata[2] = idFamily;
	         	         idModel = getDomainIdByDomainDescription (securitySession,metadata[3],idModel);
	         	         metadata[3] = idModel;
	         	        log.info("idModel" + idModel);
	         	         idColor = getDomainIdByDomainDescription (securitySession,metadata[4], idColor);
	         	         metadata[4] = idColor;
	         	        log.info("idColor" + idColor);
	         	       idSize = getDomainIdByDomainDescription (securitySession,metadata[5],idSize);
	         	         metadata[5] = idSize;
	         	        log.info("idSize: " + idSize);
	         	       idView = getDomainIdByDomainDescription (securitySession,metadata[6], idView);
	         	         metadata[6] = idView;
	         	        log.info("idView: " + idView);
	         	        //idSapAttribute = getDomainIdByDomainDescription (securitySession,metadata[8],idSapAttribute);
	         	        // metadata[8] = idSapAttribute;
	         	        // idMarket = getDomainIdByDomainDescription (securitySession,metadata[9], idMarket);
	         	        // metadata[9] = idMarket;
		          	     
	         	         /*modelspiaggio.put(1, codiceprodotto);
		          	     modelspiaggio.put(2, marca);
		          	     modelspiaggio.put(3, famiglia);
		          	     modelspiaggio.put(4, modello);
		          	     modelspiaggio.put(5, colore);	
		          	     modelspiaggio.put(6, taglia);
		          	     modelspiaggio.put(7, vista);
		          	     modelspiaggio.put(8, altezza);
		          	     modelspiaggio.put(9, larghezza);
		          	     modelspiaggio.put(10, colore);	*/
	         	         
	         	         modelspiaggio.put(1, "PIAGGIO.FIELD.PRODUCT CODE");
		          	     modelspiaggio.put(2, "PIAGGIO.FIELD.PRODUCT BRAND");
		          	     modelspiaggio.put(3, "PIAGGIO.FIELD.PRODUCT FAMILY");
		          	     modelspiaggio.put(4, "PIAGGIO.FIELD.PRODUCT MODEL");
		          	     modelspiaggio.put(5, "PIAGGIO.FIELD.ECOMM.COLOR");	
		          	     modelspiaggio.put(6, "PIAGGIO.FIELD.ECOMM.SIZE");
		          	     modelspiaggio.put(7, "PIAGGIO.FIELD.ECOMM.PRODUCT VIEW");
		          	     modelspiaggio.put(8, "PIAGGIO.FIELD.ECOMM.IMAGE HEIGHT");
		          	     modelspiaggio.put(9, "PIAGGIO.FIELD.ECOMM.IMAGE WIDTH");
		          	    
		          	     for (int i=1;i<modelspiaggio.size();i++) {
		          	    	log.info("modelpiaggio " + modelspiaggio.get(i)); 
		          	     }
		          	     
		          	     /*modelspiaggio.put(11, attributoSap);
		          	     modelspiaggio.put(12, mercato);
		          	     modelspiaggio.put(13, status);
		 
		          	     piaggioTabularFields.put(1, mercato);
		          	     setTabularFields(piaggioTabularFields);
		          	     setTabularTable(new TeamsIdentifier(marketTableTabular));
		          	     setTabularFieldId(marketTabularFieldId);*/
		          	     setCascadingFields(true);
		          	     int indexCascading []= {1,2,3};
		          	     setTypeCascading(FAMILY_BRAND_CODE);
		          	     setIndexCascadingField(indexCascading);
			          	 DomainValue element1 = new DomainValue();
			       		 DomainValue element2 = new DomainValue();
						 DomainValue element3 = new DomainValue();
						 DomainValue [] element = {element1,element2,element3};
						 setElementCascading(element);
		          	  	 populateMetadataFields (metadataFields, modelspiaggio, collection, metadata);
		          	  	 //setCascadingType(metadata,cascadingFamily,collection,FAMILY_BRAND_CODE);
	  	  			break;
	  	  			

	    		}
	          }
		}

	}
	
}
