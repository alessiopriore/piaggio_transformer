package com.piaggio.dam.transformers;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.CascadingDomainValue;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataTable;
import com.artesia.metadata.MetadataTableField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;
import com.artesia.security.SecuritySession;
import com.artesiatech.meta.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.http.*;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.StringEntity;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;


public class DamCommon {
	private static final Log log = LogFactory.getLog(DamCommon.class);
	private TeamsIdentifier TABULAR_TABLE;
	private HashMap<Integer,String> elicaTabularFieldsArray;
	private String tabularFieldId;
	private int indexCascadingField [];
	private DomainValue elementCascading [];
	private boolean isCascadingFields;

	private String typeCascading;
	private MetadataField metadataFieldOld [];
	
    public void populateMetadataFields(HashMap<Integer, MetadataField> metadataFields, HashMap<Integer, String> modelsPiaggio,
                                       MetadataCollection collection, String metadata[]) {
        //fields che leggiamo dal file di properties
        MetadataField metadataField;
        //vettore di tutti i metadatafilds oraginali dell'asset (e in caso di versione anche quelli  elica)
        MetadataField metadataField2 [] = collection.getAllMetadataFields();
        HashMap<Integer,String> tabularFields = getTabularFields();
        int dimTabularFields = 0;
        log.info("tabularFields: " + tabularFields);
        if (tabularFields != null) {
        	dimTabularFields = tabularFields.size();
        	log.info("dimTabularFields: " + dimTabularFields);
        	System.out.println("dimTabularFields: " + dimTabularFields);
        }
       if (modelsPiaggio != null) {
	        for (int i = 0; i < metadataFields.size(); i++) {
	            metadataField = metadataFields.get(i + 1); 
	            TeamsIdentifier model = new TeamsIdentifier(modelsPiaggio.get(i + 1));
	            
	            log.info("model is: " + model);
	            System.out.println("model is: " + model);
	            if (collection.findElementById(model) == null) {
	            	
	            	 if(dimTabularFields > 0 ) {
	            		
	            		for (int j = 0; j < dimTabularFields; j++) {
	            			if (modelsPiaggio.get(i + 1).equalsIgnoreCase(tabularFields.get(j + 1))) {
	            				log.info("il campo tabular � contenuto nel vettore appena creato ");
	            				System.out.println("il campo tabular � contenuto nel vettore appena creato");
	            				MetadataTable nameField =  (MetadataTable) collection.findElementById(getTabularTable());
	                     	    MetadataTableField fields2 = null;
	                     	    List <MetadataElement> elementList = new ArrayList<MetadataElement>();
	                     	    
	                     	    if (nameField == null) {
		                     	    	//creo un campo di tipo tabular
		                     	    	MetadataTable parentTable = new MetadataTable();
		                     	    	//creazione di un campo tabular che non � dimensione 
			                     	    log.info("sto creando un campo tabular");
			                     	    System.out.println("sto creando un campo tabulare");
			                     	    fields2 =  new MetadataTableField();
			                          	fields2.setValueAt(0, metadata[i]);
			                          	fields2.setId(new TeamsIdentifier(getTabularFieldId()));
			                          	elementList.add(0, fields2);
			                          	parentTable.setMetadataElements(elementList);
			                          	collection.addMetadataElement(fields2);
	                  	        }
	                     	    
	            			}else {
	            			 //else per la creazione di campi non tabular
	                   		 log.info("non � un campo tabular");
	                   		 System.out.println("non � un campo tabular ");   
	                   		 System.out.println("dimTabularFields: " + dimTabularFields);
	                		 metadataField.setId(model);
	                    	 metadataField.setValue(metadata[i]);
	                    	 System.out.println("settato il valore: " + metadata[i]);
	                    	 collection.addMetadataElement(metadataField);
	                	  }
	            		}
	             	    
	            	 }else {
	               		 log.info("non � un campo tabular ");
	               		 System.out.println("non � un campo tabular 2");          
	          			 if (isCascadingFields()) {
	          				log.info("settato il valore del cascading " + metadata[3]);
	          				System.out.println("settato il valore del cascading " + metadata[3]);
	          				setCascadingType (metadata,metadataField,collection,typeCascading);
	          				setCascadingFields(false);
	 
	          			 }else {
	          				 metadataField.setId(model);
	                    	 metadataField.setValue(metadata[i]);
	                    	 log.info("settato il valore:  " + metadata[i]);
	                    	 System.out.println("settato il valore:  " + metadata[i]);
	                    	 System.out.println("dimTabularFields: " + dimTabularFields);
	                    	 collection.addMetadataElement(metadataField);
	                    	 log.info("aggiunto alla collection:  " + metadataField);
	                    	 System.out.println("aggiunto alla collection:  " + metadataField);
	          			 }
	   				
	            		
	            	 }
	            }else {
	            	log.info("il field � contenuto nella collection");
	            	System.out.println("il field � contenuto nella collection");
	            	for (int j = 0; j < metadataField2.length; j++) {
	
	                 	if (model.equalsId(metadataField2[j].getId())) {
	               
	                 		metadataField2[j].setValue(metadata[i]);
	                    	j = metadataField2.length;
	                 	}
	                }
	
	            }
	
	        }
      }
    }
    public void  setTabularFields(HashMap<Integer,String> elicaTabularFields) {
    	 elicaTabularFieldsArray = new HashMap<Integer,String>();
    	 elicaTabularFieldsArray = elicaTabularFields;
    }
    
    public HashMap<Integer,String>  getTabularFields() {
   
   	 return elicaTabularFieldsArray;
   }
    public String getDomainIdByDomainDescription(SecuritySession securitySession, String domainDescription, String domainName){
        List<DomainValue> domVals= null;
        try {
            domVals = MetadataAdminServices.getInstance().retrieveAllDomainValuesForDomain(new TeamsIdentifier(domainName), securitySession);
            for(DomainValue val:domVals){
                if(domainDescription.equalsIgnoreCase(val.getDisplayValue())){
                    return (String) val.getFieldValue();
                }
            }
        } catch (BaseTeamsException e) {
            e.printStackTrace();
        }
        return "";
    }
    
	public TeamsIdentifier getTabularTable() {
		return TABULAR_TABLE;
	}
	public void setTabularTable(TeamsIdentifier tABULAR_TABLE) {
		TABULAR_TABLE = tABULAR_TABLE;
	}
	public String getTabularFieldId() {
		return tabularFieldId;
	}
	public void setTabularFieldId(String tabularFieldId) {
		this.tabularFieldId = tabularFieldId;
	}
	
public void setCascadingType (String [] metadata, MetadataField metadataField, MetadataCollection collection,String typeCascading) {
		
		CascadingDomainValue cascadingDomainValue = new CascadingDomainValue();
		Map<Integer, DomainValue> elementValues = new HashMap<Integer, DomainValue>();

		for (int i = 0; i<getIndexCascadingField().length; i++) {
			getElementCascading()[i].setFieldValue(metadata[getIndexCascadingField()[i]]);
			elementValues.put(i+1, getElementCascading()[i]);
			log.info("element" + getElementCascading()[i].getFieldValue());
		}
		String fieldValueString = null;
		for (int j = 0; j<getIndexCascadingField().length; j++) {
			if (getIndexCascadingField().length != j+1) {
				if (j == 0) {
					fieldValueString = metadata[getIndexCascadingField()[j]]+"^";
					log.info("fieldValueString: " + fieldValueString);
				}else {
				fieldValueString += metadata[getIndexCascadingField()[j]]+"^";
				log.info("fieldValueString: " + fieldValueString);
				}
			}
			else {
				fieldValueString += metadata[getIndexCascadingField()[j]];
				log.info("fieldValueString: " + fieldValueString);
			}
		}
		cascadingDomainValue.setFieldValue(fieldValueString);
		cascadingDomainValue.setElementValues(elementValues);

		metadataField.setId(new TeamsIdentifier(typeCascading));
		metadataField.setValue(cascadingDomainValue);
		collection.addMetadataElement(metadataField);
	}

	private void setTabularField (String metadata, MetadataField metadataField, MetadataCollection collection,MetadataTableField fields2) {
		log.info("Creo un campo di tipo tabular ");
		System.out.println("Creo un campo di tipo tabular");
		String tabularArray [] =  metadata.split("-");
		int dimArr = tabularArray.length;
		MetadataValue fieldsValueArray [] = new MetadataValue[dimArr] ;
		fields2 =  new MetadataTableField();
		fields2.setId(new TeamsIdentifier(getTabularFieldId()));
		
		for (int z = 0; z < dimArr; z++ ) {
			MetadataValue fieldsValue = new MetadataValue();
 		    fieldsValueArray [z] = fieldsValue;
			fieldsValueArray[z].setValue(tabularArray[z]);
			fields2.addValue(fieldsValueArray[z]);
		} 
		
      collection.addMetadataElement(fields2);
			
	}
	
	void setMetadataFieldOld (MetadataField[] metadataField){
		metadataFieldOld = metadataField;
	}
	MetadataField [] getMetadataFieldOld (){
		return metadataFieldOld;
	}
	public boolean isCascadingFields() {
		return isCascadingFields;
	}
	public void setCascadingFields(boolean isCascadingFields) {
		this.isCascadingFields = isCascadingFields;
	}
	public int[] getIndexCascadingField() {
		return indexCascadingField;
	}
	public void setIndexCascadingField(int indexCascadingField[]) {
		this.indexCascadingField = indexCascadingField;
	}
	public DomainValue[] getElementCascading() {
		return elementCascading;
	}
	public void setElementCascading(DomainValue elementCascading[]) {
		this.elementCascading = elementCascading;
	}
	
	public String getTypeCascading() {
		return typeCascading;
	}
	public void setTypeCascading(String typeCascading) {
		this.typeCascading = typeCascading;
	}
}
