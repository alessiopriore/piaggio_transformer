package com.piaggio.dam.transformers;

import com.artesia.asset.Asset;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.server.common.plugin.ServerPluginResources;
import com.artesia.server.transformer.BaseTransformer;
import com.artesia.server.transformer.ImportTransformationResult;
import com.artesia.server.transformer.ImportTransformer;
import com.artesia.server.transformer.TransformerAssetWrapper;
import com.artesia.transformer.TransformerArgument;
import com.artesia.transformer.TransformerInfo;
import com.artesia.security.SecuritySession;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;

public class MetadataFromFilenameImportTransformer extends BaseTransformer implements ImportTransformer {
    private static final Log log = LogFactory.getLog(MetadataFromFilenameImportTransformer.class);
    private static final String DATE_FORMAT_STR = "DATE_FORMAT";
    private static final String REGEX_MAPPING_TYPE = "REGEX";
    private static final String EMBEDDED_MAPPING_TYPE = "EMBEDDED";

    private static final String MODEL_PATH = "fileproperty/model.properties";
    private static final String METADATA_PATH = "fileproperty/metadata.properties";
    private static final String LOOKUP_PATH = "fileproperty/lookup.properties";
    private static final String PROJECT_PATH = "fileproperty/project.properties";
    private static final String TABULAR_PATH = "fileproperty/tabular.properties";

    protected static final Properties PROJECT_PROPERTIES = new Properties();

    static {
        log.error("MetadataFromFilenameImportTransformer: init of static block");
        final ClassLoader classLoader = MetadataFromFilenameImportTransformer.class.getClassLoader();
        try {
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(MODEL_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(METADATA_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(LOOKUP_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(PROJECT_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(TABULAR_PATH));
            log.error(PROJECT_PROPERTIES.stringPropertyNames());
        } catch (IOException e) {
            log.error(e);
        }
        log.error("MetadataFromFilenameImportTransformer: end init of static block");
    }

    private boolean _failTransformationOnError;
    private static List<String> supportedDateFormats = null;
    private String executable;
    private HashMap<Integer,String> AssetModel = new HashMap<Integer,String>();

    //TODO vedere se scatta alla creazione e anche alla modifica dei metadati (sembra di no)
    //TODO se si carica una nuova versione dello stesso file (versioning) va in errore:Cannot specify the same field twice. Field ELICA.BRAND.COMUNICATION.PRF was found to be a duplicate
    //forse i field devono essere prima ricancellati e poi settati in  asset.setMetadata(collection);
    public ImportTransformationResult transformAssetForImport(TransformerAssetWrapper assetWrapper, File workingDir, List<TransformerArgument> argList, ServerPluginResources resources) throws BaseTeamsException {
        log.debug("richiamato metodo del transformer custom");
        Asset asset = assetWrapper.getAsset(); 
        
        File importFile = asset.getMasterContentInfo().getFile();
        SecuritySession securitySession = resources.getSecuritySession();
        String acceSpareParts = PROJECT_PROPERTIES.getProperty("acceSpareParts");
		
		AssetModel.put(1, "PIAGGIO.MODEL.ACC.SPAREPARTS");
        
        
        log.error("file importato:" +  importFile);

        try {
            ProcessBuilder command = new ProcessBuilder("mediainfo", "--Output=XML", importFile.getAbsolutePath());
            Process p = command.start();
            p.waitFor();
            

            int returnValue = p.exitValue();
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(p.getInputStream());

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();


            //ARTESIA.EMBEDDED.FIELD.FRAME_RATE
            String fileName = FilenameUtils.removeExtension(importFile.getName());
            //fix: append " " at the end of file to manage file like 'xx_xx_ _.zip' (ends with '_')
            if(fileName.endsWith("_")){
                fileName = fileName + " ";
            }

            MetadataCollection collection = asset.getMetadata();

            Integer key = -1;
    
            boolean end = false;
            for(Integer setKey : AssetModel.keySet()) {
             
          	  if (asset.getMetadataModelId().getId().equals(AssetModel.get(setKey)) == true && !end == true) {
          		  key = setKey;
          		  end = true;
          		
          		
          		String metadata [] = fileName.split("#");
      		 
          		MetadataField metadataFieldOld [] = collection.getAllMetadataFields();
          		
				MetadataCollection collectionOld = null;
				
				if (!collection.isEmpty()) {
					 collectionOld =  collection ;
					 metadataFieldOld = collectionOld.getAllMetadataFields();
				}
				
				for (int i =0 ; i< metadataFieldOld.length; i++) {
					System.out.println("metadataField2 [ " + i + "]" + metadataFieldOld [i]);
					log.error("value: " + metadataFieldOld [i].getValue());
					log.error(": " + metadataFieldOld [i].getId());
				}
          		if (collection != null ) { //&& asset.getVersion() != 1
          			
          			collection = new MetadataCollection();
                }
          		
          		 log.error("Key: " + key);
          		switch(key) {
          			
          				case 1:
          				case 2:
          				case 3:
          				case 4:
          				case 5:
          				case 6:
          				case 7:
          				case 8:
          				case 9:
          				case 10:
          				case 27:	
          					AcceSpareParts accSpareParts = new AcceSpareParts();
          					accSpareParts.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
                            
                            break;
          	    	
			            

		          	}
          		
          	  }
  
            }
            asset.setMetadata(collection);

        } catch (InterruptedException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } 



        return new ImportTransformationResult(true);
    }
    

    public void initialize(TransformerInfo info, Map<String, String> configurationAttributesMap) {
        this.executable = super.getPortableExecutable(info.getExecutable(), configurationAttributesMap);
        log.info("Embedded Metadata extraction executable: " + this.executable);
        if (configurationAttributesMap.containsKey("FAIL_ON_ERROR")) {
            this._failTransformationOnError = "Y".equalsIgnoreCase((String)configurationAttributesMap.get("FAIL_ON_ERROR"));
        }

        if (supportedDateFormats == null && configurationAttributesMap != null && !configurationAttributesMap.isEmpty()) {
            supportedDateFormats = new ArrayList();
            List<String> attributeNames = new ArrayList();
            attributeNames.addAll(configurationAttributesMap.keySet());
            int attrCnt = configurationAttributesMap.size();

            String attribName;
            for(int index = 0; index <= attrCnt; ++index) {
                String attributeName = "DATE_FORMAT" + index;
                if (configurationAttributesMap.containsKey(attributeName)) {
                    attributeNames.remove(attributeName);
                    attribName = (String)configurationAttributesMap.get(attributeName);
                    if (attribName != null) {
                        supportedDateFormats.add(attribName);
                    }
                }
            }

            if (!attributeNames.isEmpty()) {
                Iterator attribNamesIter = attributeNames.iterator();

                while(attribNamesIter.hasNext()) {
                    attribName = (String)attribNamesIter.next();
                    if (attribName.startsWith("DATE_FORMAT")) {
                        String dateFormat = (String)configurationAttributesMap.get(attribName);
                        if (dateFormat != null) {
                            supportedDateFormats.add(dateFormat);
                        }
                    }
                }
            }
        }
    }
}
