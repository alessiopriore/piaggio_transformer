package com.piaggio.dam.transformers;

import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecuritySession;

public class Brand extends DamCommon {

	private static final Log log = LogFactory.getLog(Brand.class);
	 private MetadataField brandField = new MetadataField();
	 private MetadataField year = new MetadataField();
	 private MetadataField topology = new MetadataField();
	
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String brand = piaggioProperties.getProperty("Brand");
		log.info("prepareStructureToSetMetadataInformation-brand: " + brand);
		  String marcaBrand;
	      String anno;
	      String tipologia;
	      String idBrand;
	      String idBrandType;

	      marcaBrand = piaggioProperties.getProperty("marcaBrand");
	      anno = piaggioProperties.getProperty("annoBrand");
	      tipologia = piaggioProperties.getProperty("tipoBrand");
 	      log.info("anno: " + anno);
 	   	  idBrand= piaggioProperties.getProperty("idBrand");
 	   	  idBrandType = piaggioProperties.getProperty("idBrandType");
 	       	
 	   	  idBrand = getDomainIdByDomainDescription (securitySession,metadata[0],idBrand);
 	      metadata[0] = idBrand;
 	      idBrandType = getDomainIdByDomainDescription (securitySession,metadata[2], idBrandType);
 	      metadata[2] = idBrandType;
 	   
 	      brandField.setId(new TeamsIdentifier(marcaBrand));
 	      brandField.setValue(metadata[0]);
 	      year.setId(new TeamsIdentifier(anno));
 	      year.setValue(metadata[1]);
 	     log.info("metadata[1]: " + metadata[1]);
 	      topology.setId(new TeamsIdentifier(tipologia));
 	      topology.setValue(metadata[2]);
 		 
 	  		MetadataField  metadataField [] = {brandField,year,topology};
 	   		
 	  		for (int i =0; i<metadataField.length; i++) {
 	  			log.info("metadataFieldTest ["+i+"]" + metadataField [i].getValue().getStringValue());
 	  		}
 	  		
 	   		
 	  		AssetIdentifier idTest [] = {assetIdentifier};
 	   		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);

	}
	
}

