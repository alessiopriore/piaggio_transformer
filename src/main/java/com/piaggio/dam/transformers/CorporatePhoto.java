package com.piaggio.dam.transformers;

import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecuritySession;

public class CorporatePhoto extends DamCommon{

	private static final Log log = LogFactory.getLog(CorporatePhoto.class);
	 private MetadataField brandField = new MetadataField();
	 private MetadataField year = new MetadataField();
	 private MetadataField subject = new MetadataField();
	
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String corporatePhoto = piaggioProperties.getProperty("cFoto");
		log.info("prepareStructureToSetMetadataInformation-corporatePhoto: " + corporatePhoto);
		  String marcaBrand;
	      String anno;
	      String soggetto;
	      String idBrand;
	  

	      marcaBrand = piaggioProperties.getProperty("marcaBrand");
	      anno = piaggioProperties.getProperty("annoBrand");
	      soggetto = piaggioProperties.getProperty("soggetto");
 	      log.info("anno: " + anno);
 	   	  idBrand= piaggioProperties.getProperty("idBrand");
 	       	
 	   	  idBrand = getDomainIdByDomainDescription (securitySession,metadata[0],idBrand);
 	      metadata[0] = idBrand;
 	     
 	      brandField.setId(new TeamsIdentifier(marcaBrand));
 	      brandField.setValue(metadata[0]);
 	      year.setId(new TeamsIdentifier(anno));
 	      year.setValue(metadata[1]);
 	     log.info("metadata[1]: " + metadata[1]);
 	      subject.setId(new TeamsIdentifier(soggetto));
 	      subject.setValue(metadata[2]);
 		 
 	  		MetadataField  metadataField [] = {brandField,year,subject};
 	   		
 	  		for (int i =0; i<metadataField.length; i++) {
 	  			log.info("metadataFieldTest ["+i+"]" + metadataField [i].getValue().getStringValue());
 	  		}
 	  		
 	   		
 	  		AssetIdentifier idTest [] = {assetIdentifier};
 	   		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);

	}
	
}

