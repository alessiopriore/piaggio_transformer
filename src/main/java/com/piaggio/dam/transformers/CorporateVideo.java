package com.piaggio.dam.transformers;

import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecuritySession;

public class CorporateVideo extends DamCommon{

	private static final Log log = LogFactory.getLog(CorporateVideo.class);
	 private MetadataField brandField = new MetadataField();
	 private MetadataField year = new MetadataField();
	 private MetadataField subject = new MetadataField();
	 private MetadataField language = new MetadataField();
	 
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String corporateVideo = piaggioProperties.getProperty("cVideo");
		log.info("prepareStructureToSetMetadataInformation-corporateVideo: " + corporateVideo);
		  String marcaBrand;
	      String anno;
	      String soggetto;
	      String lingua;
	      String idBrand;
	      String idlanguage;
	  

	      marcaBrand = piaggioProperties.getProperty("marcaBrand");
	      anno = piaggioProperties.getProperty("annoBrand");
	      soggetto = piaggioProperties.getProperty("soggetto");
	      lingua = piaggioProperties.getProperty("cVideoLin");
 	      
 	   	  idBrand= piaggioProperties.getProperty("idBrand");
 	   	  idlanguage =  "PIAGGIO.LOOKUP.LANGUAGE";//piaggioProperties.getProperty("idlanguage");
 	   	  idBrand = getDomainIdByDomainDescription (securitySession,metadata[0],idBrand);
 	      metadata[0] = idBrand;
 	      idlanguage = getDomainIdByDomainDescription (securitySession,metadata[3],idlanguage);
	      metadata[3] = idlanguage;
 	      
 	      brandField.setId(new TeamsIdentifier(marcaBrand));
 	      brandField.setValue(metadata[0]);
 	      year.setId(new TeamsIdentifier(anno));
 	      year.setValue(metadata[1]);
 	      log.info("metadata[1]: " + metadata[1]);
 	      subject.setId(new TeamsIdentifier(soggetto));
 	      subject.setValue(metadata[2]);
 	      language.setId(new TeamsIdentifier(lingua));
 	      language.setValue(metadata[3]);
 	  	  MetadataField  metadataField [] = {brandField,year,subject,language};
 	   		
 	  		for (int i =0; i<metadataField.length; i++) {
 	  			log.info("metadataFieldTest ["+i+"]" + metadataField [i].getValue().getStringValue());
 	  		}
 	  		
 	   		
 	  		AssetIdentifier idTest [] = {assetIdentifier};
 	   		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);

	}
	
}

