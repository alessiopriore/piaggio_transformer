package com.piaggio.dam.transformers;

import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecuritySession;

public class TechnicalSheet extends DamCommon {

	private static final Log log = LogFactory.getLog(TechnicalSheet.class);
	private HashMap<Integer,String> technicalSheetModel = new HashMap<Integer,String>();
	 private MetadataField productCode = new MetadataField();
	 private MetadataField family = new MetadataField();
	 private MetadataField model = new MetadataField();
	 private MetadataField brand = new MetadataField();
	 private MetadataField color = new MetadataField();
	 private MetadataField size = new MetadataField();
	 private MetadataField language = new MetadataField();
	 private MetadataField sapAttribute = new MetadataField();
	 private MetadataField status = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();
	
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String technicalSheet = piaggioProperties.getProperty("technicalSheet");
		log.info("prepareStructureToSetMetadataInformation-technicalSheet");
		technicalSheetModel.put(1,technicalSheet);
		
		
		Integer key = -1;
		boolean end = false;
		int numberOfPcmMetadata = 0;
		for(Integer setKey : technicalSheetModel.keySet()) {
			
			if (modelRead.equals(technicalSheetModel.get(setKey)) == true && !end == true) {
	    		  key = setKey;
	    		  end = true;
	    		  
	    		  String codiceprodotto;
         	  	  String marca;
         	      String famiglia;
         	      String modello;
         	      String colore;
         	      String taglia;
         	      String lingua;         	      
         	      String attributoSap;
         	      String mercato;
         	      String status;
         	      String idColor;
         	      String idSize;
         	      String idLanguage;
         	      String idSapAttribute;
         	      String idMarket;
         	      String idFamily;
         	      String idModel;
         	      String idBrand;
         	      String metadataWithSapAttribute [] = new String[metadata.length+1];
         	      
         	 switch(key) {
	    			
	  	  			case 1:	
	
		
		          	  	 codiceprodotto = piaggioProperties.getProperty("codiceprodotto");
		          	  	 marca = piaggioProperties.getProperty("marca");
		          	  	 famiglia = piaggioProperties.getProperty("famiglia");
		          	     modello = piaggioProperties.getProperty("modello");
		          	     colore = piaggioProperties.getProperty("colore");
		          	     taglia = piaggioProperties.getProperty("taglia");
		          	     lingua = piaggioProperties.getProperty("lingua");
		          	     attributoSap = piaggioProperties.getProperty("attributoSap");  	     
		          	     String cascading = piaggioProperties.getProperty("family_brand_code");
		          	   
			          	idColor = piaggioProperties.getProperty("idColor");
			          	idSize = piaggioProperties.getProperty("idSize");
			          	idLanguage = piaggioProperties.getProperty("idLanguage");
			          	idFamily =piaggioProperties.getProperty("idFamily");
			          	idModel= piaggioProperties.getProperty("idModel");
			          	idBrand= piaggioProperties.getProperty("idBrand");
			          	idSapAttribute= piaggioProperties.getProperty("idSapAttribute");
	
			          	 idBrand = getDomainIdByDomainDescription (securitySession,metadata[1],idBrand);
	         	         metadata[1] = idBrand;
	         	         idFamily = getDomainIdByDomainDescription (securitySession,metadata[2], idFamily);
	         	         metadata[2] = idFamily;
	         	         idModel = getDomainIdByDomainDescription (securitySession,metadata[3],idModel);
	         	         metadata[3] = idModel;
	         	         idColor = getDomainIdByDomainDescription (securitySession,metadata[4], idColor);
	         	         metadata[4] = idColor;
	         	         idSize = getDomainIdByDomainDescription (securitySession,metadata[5],idSize);
	         	         metadata[5] = idSize;
	         	         idLanguage = getDomainIdByDomainDescription (securitySession,metadata[6], idLanguage);
	         	         metadata[6] = idLanguage;
	         	      
	         	         idSapAttribute = getDomainIdByDomainDescription (securitySession,"detail",idSapAttribute);
	         	         metadataWithSapAttribute[metadata.length] = idSapAttribute;
	         	         
	         	         productCode.setId(new TeamsIdentifier(codiceprodotto));
	 		  		 	 productCode.setValue(metadata[0]);
			  		 	 color.setId(new TeamsIdentifier(colore));
			  		 	 color.setValue(metadata[4]);
			  		 	 size.setId(new TeamsIdentifier(taglia));
			  		 	 size.setValue(metadata[5]);
			  		 	 language.setId(new TeamsIdentifier(lingua));
			  		 	 language.setValue(metadata[6]);
			  		 	 sapAttribute.setId(new TeamsIdentifier(attributoSap));
			  		 	 sapAttribute.setValue(idSapAttribute);

			  	  		 String fieldValueString = metadata[1]+ "^" + metadata[2] + "^" + metadata[3];
			  	   		 cascadingFamily.setId(new TeamsIdentifier(cascading));
			  	   		 cascadingFamily.setValue(fieldValueString);
			  	   		
			  	  		MetadataField  metadataField [] = {productCode,color,language,size,sapAttribute,cascadingFamily};
			 
			  	  		AssetIdentifier idTest [] = {assetIdentifier};
			  	   		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
		  	   			 
	  	  			break;
	  	  			

	    		}
	          }
		}

	}

}