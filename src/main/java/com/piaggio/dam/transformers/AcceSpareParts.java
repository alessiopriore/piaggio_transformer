package com.piaggio.dam.transformers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.artesia.security.SecuritySession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.CascadingDomainValue;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;

public class AcceSpareParts extends DamCommon {

	private static final Log log = LogFactory.getLog(AcceSpareParts.class);
	private HashMap<Integer,String> acceSparePartsModel = new HashMap<Integer,String>();
	 private MetadataField productCode = new MetadataField();
	 private MetadataField color = new MetadataField();
	 private MetadataField size = new MetadataField();
	 private MetadataField view = new MetadataField();
	 private MetadataField height = new MetadataField();
	 private MetadataField width = new MetadataField();
	 private MetadataField sapAttribute = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();
	
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String acceSpareParts = piaggioProperties.getProperty("acceSpareParts");
		log.info("prepareStructureToSetMetadataInformation-acceSpareParts: " + acceSpareParts);
		acceSparePartsModel.put(1,acceSpareParts);
		
		
		Integer key = -1;
		boolean end = false;
		
		for(Integer setKey : acceSparePartsModel.keySet()) {
			
			if (modelRead.equals(acceSparePartsModel.get(setKey)) == true && !end == true) {
	    		  key = setKey;
	    		  end = true;
	    		  
	    		  String codiceprodotto;
         	      String colore;
         	      String taglia;
         	      String vista;
         	      String altezza;
         	      String larghezza;
         	      String attributoSap;
         	      String idColor;
         	      String idSize;
         	      String idView;
         	      String idSapAttribute;
         	      String idFamily;
         	      String idModel;
         	      String idBrand;
         	      String metadataWithSapAttribute = new String();
         	      boolean isFront = false;
         	      
         	 switch(key) {
	    			
	  	  			case 1:	
	  	  				
	  	  			
	  	   		
		  	   		for (int i=0; i<metadata.length; i++) {
		  	   			log.info("metadata []: " + metadata [i]);
		  	   		}
		  	   		codiceprodotto = piaggioProperties.getProperty("codiceprodotto");
		  	   	    colore = piaggioProperties.getProperty("colore");
		  	   	  	taglia = piaggioProperties.getProperty("taglia");
		  	   	    vista = piaggioProperties.getProperty("vista");
		  	   	  	altezza = piaggioProperties.getProperty("altezza");
		  	   	  	larghezza = piaggioProperties.getProperty("larghezza");
		  	   	  	attributoSap = piaggioProperties.getProperty("attributoSap");
		  	   	    String cascading = piaggioProperties.getProperty("family_brand_code");
		  	   	    
		  	   	  	 idColor = piaggioProperties.getProperty("idColor");
		  	   	  	 idSize = piaggioProperties.getProperty("idSize");
		  	   	  	 idView = piaggioProperties.getProperty("idView");
		  	   	  	 idFamily =piaggioProperties.getProperty("idFamily");
		  	   	  	 idModel= piaggioProperties.getProperty("idModel");
		  	   	  	 idBrand= piaggioProperties.getProperty("idBrand");
		  	   	  	 idSapAttribute = piaggioProperties.getProperty("idSapAttribute");
		  	       	
		  	   	  
		  	   	  	
		  	       	   idBrand = getDomainIdByDomainDescription (securitySession,metadata[1],idBrand);
		  	           metadata[1] = idBrand;
		  	           idFamily = getDomainIdByDomainDescription (securitySession,metadata[2], idFamily);
		  	           metadata[2] = idFamily;
		  	           idModel = getDomainIdByDomainDescription (securitySession,metadata[3],idModel);
		  	           metadata[3] = idModel;
		  	           
		  	           idColor = getDomainIdByDomainDescription (securitySession,metadata[4], idColor);
		  	           metadata[4] = idColor;
		  	           log.info("metadata[4]" + metadata[4]);
		  	           idSize = getDomainIdByDomainDescription (securitySession,metadata[5],idSize);
		  	           metadata[5] = idSize;
		  	           log.info("metadata[5] " + metadata[5] );
		  	           isFront = metadata[6].equalsIgnoreCase("FRONT");
		  	           idView = getDomainIdByDomainDescription (securitySession,metadata[6], idView);
		  	           metadata[6] = idView;
		  	           log.info("metadata[6] " + metadata[6]);
		  	          
			  	  		if (isFront) {
			  	  			 idSapAttribute = getDomainIdByDomainDescription (securitySession,"picture", idSapAttribute);
			  	  			 metadataWithSapAttribute= idSapAttribute;
			  	  		 }else {
			  	  			 idSapAttribute = getDomainIdByDomainDescription (securitySession,"galleryImages", idSapAttribute);
			  	  			 metadataWithSapAttribute= idSapAttribute;
			  	  		 }
		  	  		log.info("metadataWithSapAttribute: " + metadataWithSapAttribute);
		  	  		log.info("idColor: " + idColor);
		  	   	  	log.info("idSize: " + idSize);
		  	   	  	log.info("idView: " + idView);
		  	   	  	log.info("idSapAttribute: " + idSapAttribute);
		  	 	 
		  		 	productCode.setId(new TeamsIdentifier(codiceprodotto));
		  		 	productCode.setValue(metadata[0]);
		  		 	color.setId(new TeamsIdentifier(colore));
		  		 	color.setValue(metadata[4]);
		  		 	size.setId(new TeamsIdentifier(taglia));
		  		 	size.setValue(metadata[5]);
		  		 	view.setId(new TeamsIdentifier(vista));
		  		 	view.setValue(metadata[6]);
		  		 	sapAttribute.setId(new TeamsIdentifier(attributoSap));
		  		 	sapAttribute.setValue(metadataWithSapAttribute);
		  		 	height.setId(new TeamsIdentifier(altezza));
		  		 	height.setValue(metadata[7]);
		  		 	width.setId(new TeamsIdentifier(larghezza));
		  		 	width.setValue(metadata[8]);
		  		 	
		  	  		String fieldValueString = metadata[1]+"^" + metadata[2] + "^" + metadata[3];
		  	   		
		  	   		cascadingFamily.setId(new TeamsIdentifier(cascading));
		  	   		cascadingFamily.setValue(fieldValueString);
		  	   		
		  	  		MetadataField  metadataField [] = {productCode,color,size, view, sapAttribute,height,width,cascadingFamily};
		  	   		
		  	  		for (int i =0; i<metadataField.length; i++) {
		  	  			log.info("metadataFieldTest ["+i+"]" + metadataField [i].getValue().getStringValue());
		  	  		}
		  	  		
		  	   		
		  	  		AssetIdentifier idTest [] = {assetIdentifier};
		  	   		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
	  	   		
	  	  			break;
	  	  			

	    		}
	          }
		}

	}
	
}
