package com.piaggio.dam.transformers;

import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecuritySession;

public class Vehicle extends DamCommon{
	private static final Log log = LogFactory.getLog(Vehicle.class);
	private HashMap<Integer,String> vehicleModel = new HashMap<Integer,String>();
	 private MetadataField productCode = new MetadataField();
	 private MetadataField family = new MetadataField();
	 private MetadataField model = new MetadataField();
	 private MetadataField brand = new MetadataField();
	 private MetadataField motorization = new MetadataField();
	 private MetadataField color = new MetadataField();
	 private MetadataField year = new MetadataField();
	 private MetadataField market = new MetadataField();
	 private MetadataField mediaType = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();

	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String vehicle = piaggioProperties.getProperty("vehicle");
		log.info("prepareStructureToSetMetadataInformation-vehicle");
		vehicleModel.put(1,vehicle);
		
		
		Integer key = -1;
		boolean end = false;
		int numberOfPcmMetadata = 0;
		for(Integer setKey : vehicleModel.keySet()) {
			
			if (modelRead.equals(vehicleModel.get(setKey)) == true && !end == true) {
	    		  key = setKey;
	    		  end = true;
	   
	    		  String codiceprodotto;
         	  	  String marca;
         	      String famiglia;
         	      String modello;
         	      String motorizzazione;
         	      String colore;     	      
        	      String mercato;
         	      String annoVeicolo;        	      
         	      String tipoMedia;
         	      String idFamily;
         	      String idModel;
         	      String idBrand;
         	      String idMediaType;
         	      String idMarket;
         	      String  idColor;
         	     String  idmotorizzazione;
         	   
         	      
         	 switch(key) {
	    			
	  	  			case 1:	
		          		
		          	  	 codiceprodotto = piaggioProperties.getProperty("codiceprodotto");
		          	  	 marca = piaggioProperties.getProperty("marca");
		          	  	 famiglia = piaggioProperties.getProperty("famiglia");
		          	     modello = piaggioProperties.getProperty("modello");
		          	     motorizzazione = piaggioProperties.getProperty("motorizzazione");
		          	     colore = piaggioProperties.getProperty("colore");
		          	     annoVeicolo = piaggioProperties.getProperty("annoVeicolo");
		          	     mercato = piaggioProperties.getProperty("mercato");
		          	     tipoMedia = piaggioProperties.getProperty("tipoMedia");
		          	     String cascading = piaggioProperties.getProperty("family_brand_code_vehic");
		          	   
		          	     idFamily =piaggioProperties.getProperty("idVehicleFamily");
		          	     idModel= piaggioProperties.getProperty("idVehicleModel");
		          	     idBrand= piaggioProperties.getProperty("idBrand");
		          	     idMediaType = piaggioProperties.getProperty("idMediaType");
		          	     idMarket = piaggioProperties.getProperty("idMarket");
		          	     idColor = piaggioProperties.getProperty("idVehicleColor");
		          	     idmotorizzazione =piaggioProperties.getProperty("idMotorizzazione");
		          	     log.info("idmotorizzazione" + idmotorizzazione);
		          	     
			          	 idBrand = getDomainIdByDomainDescription (securitySession,metadata[1],idBrand);
	         	         metadata[1] = idBrand;
	         	         idFamily = getDomainIdByDomainDescription (securitySession,metadata[2], idFamily);
	         	         metadata[2] = idFamily;
	         	         idModel = getDomainIdByDomainDescription (securitySession,metadata[3],idModel);
	         	         metadata[3] = idModel;
	         	         idmotorizzazione= getDomainIdByDomainDescription (securitySession,metadata[4],idmotorizzazione);
	         	         metadata[4] = idmotorizzazione;
	         	         idColor = getDomainIdByDomainDescription (securitySession,metadata[5],idColor);
	         	         metadata[5] = idColor;
	         	         
	         	         idMarket = getDomainIdByDomainDescription (securitySession,metadata[7],idMarket);
	         	         metadata[7] = idMarket;
	         	         idMediaType = getDomainIdByDomainDescription (securitySession,metadata[8],idMediaType);
	         	         metadata[8] = idMediaType;
	         	         
	         	         
	         	        
	         	         productCode.setId(new TeamsIdentifier(codiceprodotto));
	 		  		 	 productCode.setValue(metadata[0]);
	 		  		 	 motorization.setId(new TeamsIdentifier(motorizzazione));
			  		 	 motorization.setValue(metadata[4]);
			  		 	 color.setId(new TeamsIdentifier(colore));
			  		 	 color.setValue(metadata[5]);
			  		 	 year.setId(new TeamsIdentifier(annoVeicolo));
			  		 	 year.setValue(metadata[6]);
			  		 	 market.setId(new TeamsIdentifier(mercato));
			  		 	 market.setValue(metadata[7]);
			  		 	 mediaType.setId(new TeamsIdentifier(tipoMedia));
			  		 	 mediaType.setValue(metadata[8]);
			  		 	 
			  	  		 String fieldValueString = metadata[1]+ "^" + metadata[2] + "^" + metadata[3];
			  	   		 cascadingFamily.setId(new TeamsIdentifier(cascading));
			  	   		 cascadingFamily.setValue(fieldValueString);
			  	   		
			  	  		MetadataField  metadataField [] = {productCode,color,motorization,year,market,mediaType,cascadingFamily};
			 
			  	  		AssetIdentifier idTest [] = {assetIdentifier};
			  	   		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
		  	   			 

		          	  	 
	  	  			break;
	  	  			

	    		}
	          }
		}

	}
}
