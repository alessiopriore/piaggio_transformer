package com.piaggio.dam.transformers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.artesia.security.SecuritySession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.CascadingDomainValue;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;

public class RANetworkStand extends DamCommon {

	private static final Log log = LogFactory.getLog(RANetworkStand.class);
	private HashMap<Integer,String> acceSparePartsModel = new HashMap<Integer,String>();
	 private MetadataField brand = new MetadataField();
	 private MetadataField category = new MetadataField();
	 private MetadataField type = new MetadataField();
	 private MetadataField year = new MetadataField();
	 private MetadataField language = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();
	
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String rANetworkStand = piaggioProperties.getProperty("RANetworkStand");
		log.info("prepareStructureToSetMetadataInformation-RANetworkStand: " + rANetworkStand);
		
		String marca;
		String categoria;
		String tipo;
		String anno;
		String lingua;
		String idRBrand;
	    String idRCategory;
	    String idRTypes;	
	    String idLingua;
	    
	    marca = piaggioProperties.getProperty("ransBrand");	    
	    categoria = piaggioProperties.getProperty("ransCategory");
	    tipo = piaggioProperties.getProperty("ransType");
	    anno = piaggioProperties.getProperty("ransYear");
	    lingua = piaggioProperties.getProperty("ransLanguage");      
	    		
	    idRBrand =piaggioProperties.getProperty("idRBrand");
	    idRCategory= piaggioProperties.getProperty("idRCategory");
	    idRTypes= piaggioProperties.getProperty("idRTypes"); 				
	   	idLingua= piaggioProperties.getProperty("idLanguage"); 	
	   	log.info("idLingua: " + idLingua);
	   	//idLingua = "PIAGGIO.LOOKUP.LANGUAGE";
	   	idRBrand = getDomainIdByDomainDescription (securitySession,metadata[0],idRBrand);
        metadata[0] = idRBrand;
        idRCategory = getDomainIdByDomainDescription (securitySession,metadata[1], idRCategory);
        metadata[1] = idRCategory;
        idRTypes = getDomainIdByDomainDescription (securitySession,metadata[2],idRTypes);
        metadata[2] = idRTypes;  	   	  
        idLingua = getDomainIdByDomainDescription (securitySession,metadata[4],idLingua);
        metadata[4] = idLingua; 
        
        brand.setId(new TeamsIdentifier(marca));
        brand.setValue(metadata[0]);
        category.setId(new TeamsIdentifier(categoria));
        category.setValue(metadata[1]);
        type.setId(new TeamsIdentifier(tipo));
        type.setValue(metadata[2]);
        year.setId(new TeamsIdentifier(anno));
        year.setValue(metadata[3]);
		language.setId(new TeamsIdentifier(lingua));
		language.setValue(metadata[4]);
		  	   		
		MetadataField  metadataField [] = {brand,category,type,year,language};
		  
		AssetIdentifier idTest [] = {assetIdentifier};
		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
	  }
	
}
