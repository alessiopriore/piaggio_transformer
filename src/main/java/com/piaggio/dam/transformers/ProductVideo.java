package com.piaggio.dam.transformers;

import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecuritySession;

public class ProductVideo extends DamCommon{
	private static final Log log = LogFactory.getLog(ProductVideo.class);
	private HashMap<Integer,String> productVideoModel = new HashMap<Integer,String>();
	 private MetadataField productCode = new MetadataField();
	 private MetadataField color = new MetadataField();
	 private MetadataField language = new MetadataField();
	 private MetadataField year= new MetadataField();
	 private MetadataField sapAttribute = new MetadataField();

	 private MetadataField cascadingFamily = new MetadataField();
	 
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation ( String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],AssetIdentifier idAsset, SecuritySession securitySession) throws BaseTeamsException {
		
		String productVideo = piaggioProperties.getProperty("productVideo");
		log.info("prepareStructureToSetMetadataInformation: " + productVideo);
		productVideoModel.put(1,productVideo);
		
		
		Integer key = -1;
		boolean end = false;
		log.info("productVideoModel.keySet(): " +productVideoModel.keySet());
		for(Integer setKey : productVideoModel.keySet()) {
			log.info("productVideoModel.get(setKey): " +productVideoModel.get(setKey));
			log.info("assetIdentifier.equals(productVideoModel.get(setKey)): " + modelRead.equals(productVideoModel.get(setKey)));
			if (modelRead.equals(productVideoModel.get(setKey)) == true && !end == true) {
	    		  key = setKey;
	    		  end = true;
	    		 
	    		  
	    		  String codiceprodotto;
         	  	  String marca;
         	      String famiglia;
         	      String modello;
         	      String lingua;
         	      String annoVideo;        	      
         	      String attributoSap;     	      
         	      String idLanguage;
         	      String idSapAttribute;
         	      String idFamily;
        	      String idModel;
        	      String idBrand;
         	      
         	 switch(key) {
	    			
	  	  			case 1:		  	  
	  	  	
	  	  			codiceprodotto = piaggioProperties.getProperty("codiceprodotto");
	          	  	 marca = piaggioProperties.getProperty("marca");
	          	  	 famiglia = piaggioProperties.getProperty("famiglia");
	          	     modello = piaggioProperties.getProperty("modello");
	          	     lingua = piaggioProperties.getProperty("lingua");
	          	     annoVideo = piaggioProperties.getProperty("annoVideo");
	          	     attributoSap = piaggioProperties.getProperty("attributoSap");
	          	     String cascading = piaggioProperties.getProperty("family_brand_code");
	          	     idFamily =piaggioProperties.getProperty("idFamily");
		  	   	  	 idModel= piaggioProperties.getProperty("idModel");
		  	   	  	 idBrand= piaggioProperties.getProperty("idBrand");
	          	     idLanguage = piaggioProperties.getProperty("idLanguage");
	          	     idSapAttribute= piaggioProperties.getProperty("idSapAttribute");
	          	   
        	         idLanguage = getDomainIdByDomainDescription (securitySession,metadata[4], idLanguage);
        	         metadata[4] = idLanguage;
        	         idSapAttribute = getDomainIdByDomainDescription (securitySession,"detail",idSapAttribute);
        	         idBrand = getDomainIdByDomainDescription (securitySession,metadata[1],idBrand);
		  	         metadata[1] = idBrand;
		  	         idFamily = getDomainIdByDomainDescription (securitySession,metadata[2], idFamily);
		  	         metadata[2] = idFamily;
		  	         idModel = getDomainIdByDomainDescription (securitySession,metadata[3],idModel);
		  	         metadata[3] = idModel;
		  	           
        	         productCode.setId(new TeamsIdentifier(codiceprodotto));
 		  		 	 productCode.setValue(metadata[0]);
		  		 	 language.setId(new TeamsIdentifier(lingua));
		  		 	 language.setValue(metadata[4]);
		  		 	 year.setId(new TeamsIdentifier(annoVideo));
		  		 	 year.setValue(metadata[5]);
		  		 	 sapAttribute.setId(new TeamsIdentifier(attributoSap));
		  		 	 sapAttribute.setValue(idSapAttribute);

		  	  		 String fieldValueString = metadata[1]+ "^" + metadata[2] + "^" + metadata[3];
		  	   		 cascadingFamily.setId(new TeamsIdentifier(cascading));
		  	   		 cascadingFamily.setValue(fieldValueString);
		  	   		
		  	  		MetadataField  metadataField [] = {productCode,language,year,sapAttribute,cascadingFamily};
		  	   		
		  	  		for (int i =0; i<metadataField.length; i++) {
		  	  		    log.info("metadataField.length " + metadataField.length);
		  	  			log.info("metadataFieldTest ["+i+"]" + metadataField [i].getValue().getStringValue());
		  	  		}
		  
		  	  		AssetIdentifier idTest [] = {assetIdentifier};
		  	   		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
	  	   			 
	  	  			break;
	  	  			

	    		}
	          }
		}

	}
}
