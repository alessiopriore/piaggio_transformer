package com.piaggio.dam.transformers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.artesia.security.SecuritySession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.CascadingDomainValue;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;

public class Guidelines extends DamCommon {

	private static final Log log = LogFactory.getLog(Guidelines.class);
	private HashMap<Integer,String> acceSparePartsModel = new HashMap<Integer,String>();
	 private MetadataField typeInst = new MetadataField();
	 private MetadataField year = new MetadataField();
	 private MetadataField market = new MetadataField();
	 
	 private MetadataField cascadingFamily = new MetadataField();
	
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String techDrawing = piaggioProperties.getProperty("techDrawing");
		log.info("prepareStructureToSetMetadataInformation-techDrawing: " + techDrawing);
		
		String tInstalla;	
		String retailMarket;	
		String annoRetail;	
		
		String idFamily;
	    String idModel;
	    String idBrand;		  
	    String idTypeInstall;	
	    String idMarket;
	    
	    tInstalla = piaggioProperties.getProperty("tInstalla");
	    retailMarket = piaggioProperties.getProperty("retailMarket");
	    annoRetail = piaggioProperties.getProperty("annoRetail");

	    String cascading = piaggioProperties.getProperty("family_brand_code_vehic"); 	      
         	     
	    idFamily =piaggioProperties.getProperty("idVehicleFamily");
	   	idModel= piaggioProperties.getProperty("idVehicleModel");
	   	idBrand= piaggioProperties.getProperty("idBrand"); 				
	   	idTypeInstall = piaggioProperties.getProperty("idTypeInstall"); 
	   	idMarket = piaggioProperties.getProperty("idMarket"); 
	   	
	   	idBrand = getDomainIdByDomainDescription (securitySession,metadata[0],idBrand);
        metadata[0] = idBrand;
        idFamily = getDomainIdByDomainDescription (securitySession,metadata[1], idFamily);
        metadata[1] = idFamily;
        idModel = getDomainIdByDomainDescription (securitySession,metadata[2],idModel);
        metadata[2] = idModel;  	   	  
        idTypeInstall = getDomainIdByDomainDescription (securitySession,metadata[3],idTypeInstall);
        metadata[3] = idTypeInstall;
        idMarket = getDomainIdByDomainDescription (securitySession,metadata[5],idMarket);
        metadata[5] = idMarket;
        
        typeInst.setId(new TeamsIdentifier(tInstalla));
        typeInst.setValue(metadata[3]);
        year.setId(new TeamsIdentifier(annoRetail));
        year.setValue(metadata[4]);
        market.setId(new TeamsIdentifier(retailMarket));
        market.setValue(metadata[5]);
        
		String fieldValueString = metadata[0]+"^" + metadata[1] + "^" + metadata[2];
		  	   		
		cascadingFamily.setId(new TeamsIdentifier(cascading));
		cascadingFamily.setValue(fieldValueString);
		  	   		
		MetadataField  metadataField [] = {typeInst,year,market,cascadingFamily};
		  
		AssetIdentifier idTest [] = {assetIdentifier};
		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
	  }
	
}
