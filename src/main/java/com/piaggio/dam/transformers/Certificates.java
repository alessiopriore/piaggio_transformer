package com.piaggio.dam.transformers;

import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecuritySession;

public class Certificates extends DamCommon{
	private static final Log log = LogFactory.getLog(Certificates.class);
	private HashMap<Integer,String> certificateModel = new HashMap<Integer,String>();
	 private MetadataField id = new MetadataField();
	 private MetadataField name = new MetadataField();
	 private MetadataField family = new MetadataField();
	 private MetadataField model = new MetadataField();
	 private MetadataField brand = new MetadataField();	
	 private MetadataField year = new MetadataField();
	 private MetadataField marketCustomer = new MetadataField();
	 private MetadataField language = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();

	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead, AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String certificate = piaggioProperties.getProperty("certificate");
		log.info("prepareStructureToSetMetadataInformation-certificate");
		certificateModel.put(1,certificate);
		
		
		Integer key = -1;
		boolean end = false;
		int numberOfPcmMetadata = 0;
		for(Integer setKey : certificateModel.keySet()) {
			
			if (modelRead.equals(certificateModel.get(setKey)) == true && !end == true) {
	    		  key = setKey;
	    		  end = true;
	    		  
	    		  String idCertificate;
         	  	  String marca;
         	      String famiglia;
         	      String modello;
         	      String nomeCertificate;
         	      String annoCertificate;     	      
        	      String mercatoCertificate;
        	      String lingua;
         	      String idFamily;
         	      String idModel;
         	      String idBrand;
         	      String idMercCertificato;
         	      String idLanguage;
         	      String idNomeCertificato;
         	      
         	 switch(key) {
	    			
	  	  			case 1:	
	  	  				
		          	
		          	  	 idCertificate = piaggioProperties.getProperty("idCertificate");
		          	  	 nomeCertificate = piaggioProperties.getProperty("nomeCertificate");
		          	  	 marca = piaggioProperties.getProperty("marca");
		          	  	 //famiglia = piaggioProperties.getProperty("famiglia");
		          	     //modello = piaggioProperties.getProperty("modello");
		          	     annoCertificate = piaggioProperties.getProperty("annoCertificate");
		          	     mercatoCertificate = piaggioProperties.getProperty("mercatoCertificate");
		          	     lingua = piaggioProperties.getProperty("lingua");
		          	     String cascading = piaggioProperties.getProperty("family_brand_code_vehic");
		          	     
		          	     idBrand= piaggioProperties.getProperty("idBrand");
		          	     idMercCertificato = piaggioProperties.getProperty("idMercCertificato");
		          	     idLanguage = piaggioProperties.getProperty("idLanguage");
		          	     log.info("idLanguage: " + idLanguage);
		          	     idNomeCertificato = piaggioProperties.getProperty("idNomeCertificato");
		          	 
		          	     idNomeCertificato = getDomainIdByDomainDescription (securitySession,metadata[1],idNomeCertificato);
	         	         metadata[1] = idNomeCertificato;
			          	 idBrand = getDomainIdByDomainDescription (securitySession,metadata[2],idBrand);
	         	         metadata[2] = idBrand;
	         	         idMercCertificato = getDomainIdByDomainDescription (securitySession,metadata[4],idMercCertificato);
	         	         metadata[4] = idMercCertificato;
	         	         log.info("idLanguage: " + idLanguage);
	         	         idLanguage = getDomainIdByDomainDescription (securitySession,metadata[5],idLanguage);
	         	         metadata[5] = idLanguage;
	         	         
	         	         id.setId(new TeamsIdentifier(idCertificate));
	         	         id.setValue(metadata[0]);
	         	         name.setId(new TeamsIdentifier(nomeCertificate));
	         	         name.setValue(metadata[1]);
	         	         marketCustomer.setId(new TeamsIdentifier(mercatoCertificate));
	         	         marketCustomer.setValue(metadata[4]);
			  		 	 year.setId(new TeamsIdentifier(annoCertificate));
			  		 	 year.setValue(metadata[3]);
			  		 	 language.setId(new TeamsIdentifier(lingua));
			  		 	 language.setValue(metadata[5]);
			  		 	
			  		 	 
			  	  		 String fieldValueString = metadata[2];
			  	   		 cascadingFamily.setId(new TeamsIdentifier(cascading));
			  	   		 cascadingFamily.setValue(fieldValueString);
			  	   		
			  	  		MetadataField  metadataField [] = {id,name,marketCustomer,year,language,cascadingFamily};
			 
			  	  		AssetIdentifier idTest [] = {assetIdentifier};
			  	   		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
		  	   			 
	  	  			break;
	  	  			

	    		}
	          }
		}

	}
}
