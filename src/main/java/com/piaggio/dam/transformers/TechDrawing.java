package com.piaggio.dam.transformers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.artesia.security.SecuritySession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.CascadingDomainValue;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;

public class TechDrawing extends DamCommon {

	private static final Log log = LogFactory.getLog(TechDrawing.class);
	private HashMap<Integer,String> acceSparePartsModel = new HashMap<Integer,String>();
	 private MetadataField product = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();
	
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String techDrawing = piaggioProperties.getProperty("techDrawing");
		log.info("prepareStructureToSetMetadataInformation-techDrawing: " + techDrawing);
		
		String prodotto;		  
		String idVehicleTypes;
	    String idVehicleModel;
	    String idBrand;		  
	    
	    prodotto = piaggioProperties.getProperty("prodotto");	      
	    String cascading = piaggioProperties.getProperty("family_brand_code_techDraw"); 	      
         	     
	    idVehicleTypes =piaggioProperties.getProperty("idVehicleTypes");
	    idVehicleModel= piaggioProperties.getProperty("idVehicleModel");
	   	idBrand= piaggioProperties.getProperty("idBrand"); 				
	  	  			
	   	idBrand = getDomainIdByDomainDescription (securitySession,metadata[1],idBrand);
        metadata[1] = idBrand;
        idVehicleTypes = getDomainIdByDomainDescription (securitySession,metadata[2], idVehicleTypes);
        metadata[2] = idVehicleTypes;
        idVehicleModel = getDomainIdByDomainDescription (securitySession,metadata[3],idVehicleModel);
        metadata[3] = idVehicleModel;  	   	  
		  
        product.setId(new TeamsIdentifier(prodotto));
		product.setValue(metadata[0]);
	
		String fieldValueString = metadata[1]+"^" + metadata[2] + "^" + metadata[3];
		  	   		
		cascadingFamily.setId(new TeamsIdentifier(cascading));
		cascadingFamily.setValue(fieldValueString);
		  	   		
		MetadataField  metadataField [] = {product,cascadingFamily};
		  
		AssetIdentifier idTest [] = {assetIdentifier};
		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
	  }
	
}
