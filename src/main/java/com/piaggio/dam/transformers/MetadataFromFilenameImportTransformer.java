package com.piaggio.dam.transformers;

import com.artesia.asset.Asset;
import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;
import com.artesia.server.common.plugin.ServerPluginResources;
import com.artesia.server.transformer.BaseTransformer;
import com.artesia.server.transformer.ImportTransformationResult;
import com.artesia.server.transformer.ImportTransformer;
import com.artesia.server.transformer.TransformerAssetWrapper;
import com.artesia.transformer.TransformerArgument;
import com.artesia.transformer.TransformerInfo;
import com.artesia.security.SecuritySession;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;

public class MetadataFromFilenameImportTransformer extends BaseTransformer implements ImportTransformer {
    private static final Log log = LogFactory.getLog(MetadataFromFilenameImportTransformer.class);
    private static final String DATE_FORMAT_STR = "DATE_FORMAT";
    private static final String REGEX_MAPPING_TYPE = "REGEX";
    private static final String EMBEDDED_MAPPING_TYPE = "EMBEDDED";

    private static final String MODEL_PATH = "fileproperties/model.properties";
    private static final String METADATA_PATH = "fileproperties/metadata.properties";
    private static final String LOOKUP_PATH = "fileproperties/lookup.properties";
    private static final String PROJECT_PATH = "fileproperties/project.properties";
    private static final String TABULAR_PATH = "fileproperties/tabular.properties";
    private static final String CASCADING_PATH = "fileproperties/cascading.properties";
    
    protected static final Properties PROJECT_PROPERTIES = new Properties();
    
    static {
        log.error("MetadataFromFilenameImportTransformer: init of static block");
        final ClassLoader classLoader = MetadataFromFilenameImportTransformer.class.getClassLoader();
        try {
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(MODEL_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(METADATA_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(LOOKUP_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(PROJECT_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(TABULAR_PATH));
            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(CASCADING_PATH));
            log.error(PROJECT_PROPERTIES.stringPropertyNames());
        } catch (IOException e) {
            log.error(e);
        }
        log.error("MetadataFromFilenameImportTransformer: end init of static block");
    }

    private boolean _failTransformationOnError;
    private static List<String> supportedDateFormats = null;
    private String executable;
    private HashMap<Integer,String> AssetModel = new HashMap<Integer,String>();
    HashMap<Integer,String> modelspiaggio = new HashMap<Integer,String>(); 
    TeamsIdentifier id0 = new TeamsIdentifier("PIAGGIO.FIELD.VIDEO FORMAT");
    TeamsIdentifier id1 = new TeamsIdentifier("PIAGGIO.FIELD.ECOMM.VIDEO YEAR");
    TeamsIdentifier id2 = new TeamsIdentifier("PIAGGIO.FIELD.PRODUCT CODE");
    MetadataElement test [] = new MetadataElement[3];
    
    
    //TODO vedere se scatta alla creazione e anche alla modifica dei metadati (sembra di no)
    //TODO se si carica una nuova versione dello stesso file (versioning) va in errore:Cannot specify the same field twice. Field ELICA.BRAND.COMUNICATION.PRF was found to be a duplicate
    //forse i field devono essere prima ricancellati e poi settati in  asset.setMetadata(collection);
    public ImportTransformationResult transformAssetForImport(TransformerAssetWrapper assetWrapper, File workingDir, List<TransformerArgument> argList, ServerPluginResources resources) throws BaseTeamsException {
        log.info("richiamato metodo del transformer custom");
        Asset asset = assetWrapper.getAsset(); 
        SecuritySession securitySession = resources.getSecuritySession();
        String acceSpareParts = PROJECT_PROPERTIES.getProperty("acceSpareParts");
        String technicalSheet = PROJECT_PROPERTIES.getProperty("technicalSheet");
        String productVideo = PROJECT_PROPERTIES.getProperty("productVideo");
        String vehicle = PROJECT_PROPERTIES.getProperty("vehicle");
        String certificate = PROJECT_PROPERTIES.getProperty("certificate");
        String brand = PROJECT_PROPERTIES.getProperty("brand");
        String cFoto = PROJECT_PROPERTIES.getProperty("cFoto");
        String cVideo = PROJECT_PROPERTIES.getProperty("cVideo");
        String techDraw = PROJECT_PROPERTIES.getProperty("techDraw");
        String tutorialVideo = PROJECT_PROPERTIES.getProperty("tutorialVideo");
        String techDocumentation = PROJECT_PROPERTIES.getProperty("techDocumentation");
        String motoplexProjects = PROJECT_PROPERTIES.getProperty("motoplexProjects");
        String guide = PROJECT_PROPERTIES.getProperty("guide");
        String rANetworkStand = PROJECT_PROPERTIES.getProperty("rANetworkStand");
        
		AssetModel.put(1, acceSpareParts);
		AssetModel.put(2, technicalSheet);
		AssetModel.put(3, productVideo);
		AssetModel.put(4, vehicle);
		AssetModel.put(5, certificate);
		AssetModel.put(6, brand);
		AssetModel.put(7, cFoto);
		AssetModel.put(8, cVideo);
		AssetModel.put(9, techDraw);
		AssetModel.put(10, tutorialVideo);
		AssetModel.put(11, techDocumentation);
		AssetModel.put(12, motoplexProjects);
		AssetModel.put(13, guide);
		AssetModel.put(14, rANetworkStand);
		TeamsIdentifier AssetModelArray [] = new TeamsIdentifier[15];
		
        
        //ARTESIA.EMBEDDED.FIELD.FRAME_RATE
		String fileName = FilenameUtils.removeExtension(asset.getName());
		log.info("fileName: " + fileName);
		//fix: append " " at the end of file to manage file like 'xx_xx_ _.zip' (ends with '_')
		if(fileName.endsWith("_")){
		    fileName = fileName + " ";
		}

		MetadataCollection collection;
		if (asset.getMetadata() == null) {
			log.info("collection null: ");
			collection = new MetadataCollection();
			asset.setMetadata(collection);
		}else {
			collection = asset.getMetadata();

			log.info("Else collection non nulla: " + asset.getMetadata());
		}

 	   for (int i=0; i<modelspiaggio.size(); i++) {
 	    	log.info("modelspiaggio.get(i+1): " + modelspiaggio.get(i+1));
			AssetModelArray [i] = new TeamsIdentifier (modelspiaggio.get(i+1));
			log.info("AssetModelArray [i]: " + AssetModelArray [i]);
		}
		
 	   log.info("AssetModelArray [i]: Pippo");
		
		Integer key = -1;
   
		boolean end = false;
		for(Integer setKey : AssetModel.keySet()) {
		 log.info("ID MODEL: " + asset.getMetadataModelId().getId());
		 log.info("AssetModel.get(setKey): " + AssetModel.get(setKey));
		  if (asset.getMetadataModelId().getId().equals(AssetModel.get(setKey)) == true && !end == true) {
			  key = setKey;
			  end = true;

			String metadata [] = fileName.split("#");	
			MetadataField metadataFieldOld [] ;
			MetadataCollection collectionOld;
			
			for(int i=0; i<metadata.length; i++) {
				log.info("metadata ["+ i + "]" + metadata [i]);
			}
			
			if (collection != null) {
				metadataFieldOld = collection.getAllMetadataFields();
				collectionOld = null;
				
				if (!collection.isEmpty()) {
					 collectionOld =  collection ;
					 metadataFieldOld = collectionOld.getAllMetadataFields();
				}
			}
			
			if (collection != null ) { //&& asset.getVersion() != 1
			 collection = new MetadataCollection();
		    }
			
			 log.error("Key: " + key);
			switch(key) {
				
					case 1:
						AcceSpareParts accSpareParts = new AcceSpareParts();
						accSpareParts.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(), asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		                
		                break;
		             
					case 2:	
		  				TechnicalSheet techSheet = new TechnicalSheet();
		  				techSheet.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
						
					case 3:	
						ProductVideo prodVideo = new ProductVideo();
						prodVideo.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, asset.getAssetId(), securitySession);
		            
						break;
					case 4:	
						Vehicle vehicl = new Vehicle();
						vehicl.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
					case 5:	
						Certificates certificat = new Certificates();
						certificat.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
					case 6:	
						Brand brandObj = new Brand();
						brandObj.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
						
					case 7:	
						CorporatePhoto cPhoto = new CorporatePhoto();
						cPhoto.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
					case 8:	
						CorporateVideo corpVideo = new CorporateVideo();
						corpVideo.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
						
					case 9:	
						TechDrawing techDrawing = new TechDrawing();
						techDrawing.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;	
						
					case 10:	
						TutorialV tutorialV = new TutorialV();
						tutorialV.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;	
					case 11:	
						TDocumentation tDocumentation = new TDocumentation();
						tDocumentation.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;	
					case 12:	
						MotoplexProjects mProjects = new MotoplexProjects();
						mProjects.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
					case 13:	
						Guidelines guidelines = new Guidelines();
						guidelines.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
					case 14:	
						RANetworkStand retailANS = new RANetworkStand();
						retailANS.prepareStructureToSetMetadataInformation(asset.getMetadataModelId().getId(),asset.getAssetId(), PROJECT_PROPERTIES, metadata, collection, securitySession);
		            
						break;
		      	}
			
		  }
  
		}
		//asset.setMetadata(collection); */



        return new ImportTransformationResult(true);
    }
    

    public void initialize(TransformerInfo info, Map<String, String> configurationAttributesMap) {
        this.executable = super.getPortableExecutable(info.getExecutable(), configurationAttributesMap);
        log.info("Embedded Metadata extraction executable: " + this.executable);
        if (configurationAttributesMap.containsKey("FAIL_ON_ERROR")) {
            this._failTransformationOnError = "Y".equalsIgnoreCase((String)configurationAttributesMap.get("FAIL_ON_ERROR"));
        }

        if (supportedDateFormats == null && configurationAttributesMap != null && !configurationAttributesMap.isEmpty()) {
            supportedDateFormats = new ArrayList();
            List<String> attributeNames = new ArrayList();
            attributeNames.addAll(configurationAttributesMap.keySet());
            int attrCnt = configurationAttributesMap.size();

            String attribName;
            for(int index = 0; index <= attrCnt; ++index) {
                String attributeName = "DATE_FORMAT" + index;
                if (configurationAttributesMap.containsKey(attributeName)) {
                    attributeNames.remove(attributeName);
                    attribName = (String)configurationAttributesMap.get(attributeName);
                    if (attribName != null) {
                        supportedDateFormats.add(attribName);
                    }
                }
            }

            if (!attributeNames.isEmpty()) {
                Iterator attribNamesIter = attributeNames.iterator();

                while(attribNamesIter.hasNext()) {
                    attribName = (String)attribNamesIter.next();
                    if (attribName.startsWith("DATE_FORMAT")) {
                        String dateFormat = (String)configurationAttributesMap.get(attribName);
                        if (dateFormat != null) {
                            supportedDateFormats.add(dateFormat);
                        }
                    }
                }
            }
        }
    }
    
    public String getDomainIdByDomainDescription(SecuritySession securitySession, String domainDescription, String domainName){
 	   List<DomainValue> domVals= null;
 	   try {
 	          domVals = MetadataAdminServices.getInstance().retrieveAllDomainValuesForDomain(new TeamsIdentifier(domainName), 
 	        		  securitySession);
 	         
 	          for(DomainValue val:domVals){
 	        	 log.info("val.getDisplayValue(): " + val.getDisplayValue());
 	              if(domainDescription.equalsIgnoreCase(val.getDisplayValue())){
 	            
 	                 return (String) val.getFieldValue();
 	              }
 	           }
 	    } catch (BaseTeamsException e) {
 	       e.printStackTrace();
 	    }
 	    return "";
 	}
}


