package com.piaggio.dam.transformers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.artesia.security.SecuritySession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.CascadingDomainValue;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;

public class TDocumentation extends DamCommon {

	private static final Log log = LogFactory.getLog(TDocumentation.class);
	 private MetadataField product = new MetadataField();
	 private MetadataField motorization = new MetadataField();
	 private MetadataField documType = new MetadataField();
	 private MetadataField language = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();
	
	@SuppressWarnings("null")
	public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
		
		String techDocumentation = piaggioProperties.getProperty("techDocumentation");
		log.info("prepareStructureToSetMetadataInformation-techDocumentation: " + techDocumentation);
		
		String prodotto;
		String motorizzazione;
		String doc_type;
		String ransLanguage;
		String idVehicleTypes;
	    String idVehicleModel;
	    String idBrand;	
	    String idMotorizzazione;
	    String idDoc_type;
	    String idLingua;
	    
	    prodotto = piaggioProperties.getProperty("prodotto");	    
	    ransLanguage = piaggioProperties.getProperty("ransLanguage");
	    motorizzazione = piaggioProperties.getProperty("motorizzazioneDoc");
		doc_type = piaggioProperties.getProperty("tipoDoc");
	    String cascading = piaggioProperties.getProperty("family_brand_code_techDraw"); 	      
         	     
	    idVehicleTypes =piaggioProperties.getProperty("idVehicleTypes");
	    idVehicleModel= piaggioProperties.getProperty("idVehicleModel");
	   	idBrand= piaggioProperties.getProperty("idBrand"); 	
	   	idDoc_type= piaggioProperties.getProperty("idDoc_type"); 	
	   	idMotorizzazione= piaggioProperties.getProperty("idMotorizzazione"); 	
	   	idLingua= piaggioProperties.getProperty("idLanguage"); 	
	   	log.info("idLingua: " + idLingua);
	   	idBrand = getDomainIdByDomainDescription (securitySession,metadata[1],idBrand);
        metadata[1] = idBrand;
        log.info(" metadata[1]: " +  metadata[1]);
        idVehicleTypes = getDomainIdByDomainDescription (securitySession,metadata[2], idVehicleTypes);
        metadata[2] = idVehicleTypes;
        log.info(" metadata[2]: " +  metadata[2]);
        idVehicleModel = getDomainIdByDomainDescription (securitySession,metadata[3],idVehicleModel);
        metadata[3] = idVehicleModel;  
        log.info(" metadata[3]: " +  metadata[3]);
        log.info("metadata[4]: " +  metadata[4]);
        log.info("idMotorizzazione: " +  idMotorizzazione);
        idMotorizzazione= getDomainIdByDomainDescription (securitySession,metadata[4],idMotorizzazione);
        metadata[4] = idMotorizzazione;
        log.info(" metadata[4]: " +  metadata[4]);
        log.info("metadata[5]: " +  metadata[5]);
        log.info("idDoc_type: " +  idDoc_type);
        idDoc_type = getDomainIdByDomainDescription (securitySession,metadata[5],idDoc_type);
        metadata[5] = idDoc_type;
        log.info(" metadata[5]: " +  metadata[5]);
        log.info("metadata[6]: " + metadata[6]);
        log.info("idLingua: " +  idLingua);
        idLingua = getDomainIdByDomainDescription (securitySession,metadata[6],idLingua);
        metadata[6] = idLingua; 
        log.info("metadata[6]: " + metadata[6]);
        //idLingua = "PIAGGIO.LOOKUP.LANGUAGE";
        product.setId(new TeamsIdentifier(prodotto));
		product.setValue(metadata[0]);
		documType.setId(new TeamsIdentifier(doc_type));
		documType.setValue(metadata[5]);
		motorization.setId(new TeamsIdentifier(motorizzazione));
		motorization.setValue(metadata[4]);
		language.setId(new TeamsIdentifier(ransLanguage));
		language.setValue(metadata[6]);
		String fieldValueString = metadata[1]+"^" + metadata[2] + "^" + metadata[3];
		  	   		
		cascadingFamily.setId(new TeamsIdentifier(cascading));
		cascadingFamily.setValue(fieldValueString);
		  	   		
		MetadataField  metadataField [] = {product,documType,motorization,language,cascadingFamily};
		//MetadataField  metadataField [] = {product,cascadingFamily,language};
		
		AssetIdentifier idTest [] = {assetIdentifier};
		AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
	  }
	
}
