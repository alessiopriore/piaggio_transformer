package com.piaggio.dam.transformers;

import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecuritySession;

public class MotoplexProjects extends DamCommon {

	private static final Log log = LogFactory.getLog(MotoplexProjects.class);
	private HashMap<Integer,String> technicalSheetModel = new HashMap<Integer,String>();
	 private MetadataField brand = new MetadataField();
	 private MetadataField country = new MetadataField();
	 private MetadataField city = new MetadataField();
	 private MetadataField dealer = new MetadataField();
	 private MetadataField year = new MetadataField();
	 private MetadataField material = new MetadataField();
	 private MetadataField cascadingFamily = new MetadataField();
	 
	 @SuppressWarnings("null")
	 public void prepareStructureToSetMetadataInformation (  String modelRead,AssetIdentifier assetIdentifier, Properties piaggioProperties, String metadata [],MetadataCollection collection, SecuritySession securitySession) throws BaseTeamsException {
			
			String MotoplexProjects = piaggioProperties.getProperty("MotoplexProjects");
			log.info("prepareStructureToSetMetadataInformation-MotoplexProjects: " + MotoplexProjects);
			
			String marcaBrand;
			String paese;
			String citta;
			String rivenditore;
			String anno;
			String materiale;
			String idCountry;
		    String idBrand;	
		    
		    
		    marcaBrand = piaggioProperties.getProperty("marcaBrand");	    
		    paese = piaggioProperties.getProperty("paese");
		    citta = piaggioProperties.getProperty("citta");	    
		    rivenditore = piaggioProperties.getProperty("rivenditore");
		    anno = piaggioProperties.getProperty("annoRetail");	    
		    materiale = piaggioProperties.getProperty("materiale");
		    
		    
		   	idBrand= piaggioProperties.getProperty("idBrand"); 				
		   	idCountry = piaggioProperties.getProperty("idMarket"); 		
		   	
		   	idBrand = getDomainIdByDomainDescription (securitySession,metadata[0],idBrand);
	        metadata[0] = idBrand;
	        idCountry = getDomainIdByDomainDescription (securitySession,metadata[1], idCountry);
	        metadata[1] = idCountry;
  	   		
	        brand.setId(new TeamsIdentifier(marcaBrand));
	        brand.setValue(metadata[0]);
	        country.setId(new TeamsIdentifier(paese));
	        country.setValue(metadata[1]);
	        city.setId(new TeamsIdentifier(citta));
	        city.setValue(metadata[2]);
	        dealer.setId(new TeamsIdentifier(rivenditore));
	        dealer.setValue(metadata[3]);
	        year.setId(new TeamsIdentifier(anno));
	        year.setValue(metadata[4]);
	        material.setId(new TeamsIdentifier(materiale));
	        material.setValue(metadata[5]);
			
			 
			MetadataField  metadataField [] = {brand,country,city,dealer,year,material};
			  
			AssetIdentifier idTest [] = {assetIdentifier};
			AssetMetadataServices.getInstance().saveMetadataForAssets(idTest,metadataField,securitySession);
		  
	

	}

}